const Course = require('../models/Course');

module.exports.addCourse = (reqBody) => {


	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	});

	return newCourse.save().then((course, error) => {

		if(error) {
			return false;
		} else {
			return true;
		}
	});

	// let message = Promise.resolve('User must be an ADMIN');
	// return message.then(value => {value});
};

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a Course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
	SYNTAX:
	findByIdAndUpdate(document ID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse)
		.then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
};

// Archive a Course
module.exports.archiveCourse = (data) => {
	if(data.isAdmin) {
		return Course.findByIdAndUpdate(data.id, data.body)
			.then((course, error) => {
				if(error) {
					return false;
				} else {
					return true;
				}
			});
	} else {
		let message = Promise.resolve('Only admin user can arhive a course!');
		return message.then(message => message);
	};
};