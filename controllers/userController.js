const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
			// No duplicate email found
		} else {
			return false;
		}
	})
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// bcrypt.hashSync(<dataToBeHash>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return user;
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false;
		} else {
			// compareSync(password, hashed password)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// ACTIVITY s38

module.exports.getProfile = (data) => {

	return User.findOne(data.id).then(result => {

		if(result == null) {
			return false;
		} else {
			result.password = "";
			return result;
		}
	})
}

// Enroll User to a Course
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId)
		.then(course => {
			// Adds the userId in the course's enrollees array
			course.enrollees.push({userId: data.userId});
			return course.save().then((course, error) => {
				if(error) {
					return false;
				} else {
					return true;
				};
			});
		});
		// Condition that will check if the user and course documents have been updated
		if(isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return false;
		}
} 
