const express = require('express');
const mongoose = require('mongoose');
// Allows our backend application to be available to our frontend application
const cors = require('cors');
const port = 4000;


// Routes
const userRoute = require('./routes/userRoute');
const courseRoute = require('./routes/courseRoute');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Connect MongoDB
mongoose.set('strictQuery', false);
mongoose.connect('mongodb+srv://admin123:admin098@cluster0.khqxuap.mongodb.net/B241-API?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.log.bind(console, 'Connection Error \n'));
db.once('open', () => console.log('Connected to MongoDB Atlas!'));

app.use('/users', userRoute);
app.use('/courses', courseRoute);

app.listen(port, () => 
	console.log(`API is now online on port ${port}`)
);