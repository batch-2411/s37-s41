const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');

// Route for creating a course
router.post('/', auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	if(isAdmin) {
		courseController.addCourse(req.body)
		.then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Route for Retrieving all Courses
router.get('/all', (req, res) => {
	courseController.getAllCourses()
		.then(resulFromController => res.send(resulFromController));
});

// Route for Retrieving all active courses
router.get('/active', (req, res) => {
	courseController.getActiveCourses()
		.then(resulFromController => res.send(resulFromController));
});

// Route for Retreiving specific course
router.get('/:courseId', (req, res) => {
	courseController.getCourse(req.params)
		.then(resulFromController => res.send(resulFromController));
});

// Update a Course
router.put('/:courseId', auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body)
		.then(resulFromController => res.send(resulFromController));
});

// ACTIVITY S40

// Route for Archiving a Course
router.patch('/:courseId/archive', auth.verify, (req, res) => {
	const data = {
		id: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		body: req.body
	};
	courseController.archiveCourse(data)
		.then(resulFromController => res.send(resulFromController));
})

module.exports = router;