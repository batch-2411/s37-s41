const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Route for checking if the user's email exists in our database
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body)
		.then(resulFromController => res.send(resulFromController));
});

// Route for User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body)
		.then(resulFromController => res.send(resulFromController));
})

// Route for User Authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body)
		.then(resulFromController => res.send(resulFromController));
})

// ACTIVITY s38

// Route for get user details
router.post('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData._id})
		.then(resulFromController => res.send(resulFromController));
})

// Route to Enroll User to a Course
router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};
	userController.enroll(data)
		.then(resultFromController => res.send(resultFromController));
});

module.exports = router;